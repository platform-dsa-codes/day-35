import java.util.Scanner;

class Solution {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Taking input from the user
        System.out.print("Enter the number of elements: ");
        int n = scanner.nextInt();
        int[] nums = new int[n];
        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < n; i++) {
            nums[i] = scanner.nextInt();
        }

        // Finding the next permutation
        nextPermutation(nums);

        // Displaying the next permutation
        System.out.print("The next permutation is: ");
        for (int num : nums) {
            System.out.print(num + " ");
        }

        scanner.close();
    }

    public static void nextPermutation(int[] nums) {
        int n = nums.length;

        // Find the first index i from the right such that nums[i] > nums[i-1]
        int i = n - 2;
        while (i >= 0 && nums[i] >= nums[i + 1]) {
            i--;
        }

        // If such an index i exists, find the smallest nums[j] to the right of nums[i] such that nums[j] > nums[i]
        if (i >= 0) {
            int j = n - 1;
            while (j >= 0 && nums[j] <= nums[i]) {
                j--;
            }
            swap(nums, i, j);
        }

        // Reverse the sub-array nums[i+1:]
        reverse(nums, i + 1, n - 1);
    }

    // Utility function to swap two elements in the array
    private static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    // Utility function to reverse a sub-array in the array
    private static void reverse(int[] nums, int start, int end) {
        while (start < end) {
            swap(nums, start, end);
            start++;
            end--;
        }
    }
}
