//{ Driver Code Starts
//Initial Template for Java

import java.util.*;
import java.io.*;
import java.lang.*;

class Sorting
{
    public static void main (String[] args) 
    {
        Scanner sc = new Scanner(System.in);
        long t = sc.nextLong();
        
        while(t-- > 0)
        {
            long n = sc.nextLong();
            long arr[] = new long[(int)n];
            
            for(long i = 0; i < n; i++)
             arr[(int)i] = sc.nextLong();
             
            System.out.println(new Solution().inversionCount(arr, n));
            
        }
    }
}

// } Driver Code Ends

class Solution
{
    // To store the count of inversions
    static long inversionCount;

    // arr[]: Input Array
    // N : Size of the Array arr[]
    // Function to count inversions in the array.
    static long inversionCount(long arr[], long N)
    {
        // Reset the inversion count
        inversionCount = 0;

        // Temporary array to store sorted elements during the merge process
        long[] temp = new long[(int)N];

        // Call the merge sort algorithm to count inversions
        mergeSort(arr, temp, 0, N - 1);

        // Return the inversion count
        return inversionCount;
    }

    // Merge sort algorithm with inversion count calculation
    static void mergeSort(long arr[], long temp[], long left, long right) {
        if (left < right) {
            long mid = left + (right - left) / 2;
            mergeSort(arr, temp, left, mid);
            mergeSort(arr, temp, mid + 1, right);
            merge(arr, temp, left, mid, right);
        }
    }

    // Merge function to merge two sorted subarrays and count inversions
    static void merge(long arr[], long temp[], long left, long mid, long right) {
        long i = left, j = mid + 1, k = left;

        while (i <= mid && j <= right) {
            if (arr[(int)i] <= arr[(int)j]) {
                temp[(int)k++] = arr[(int)i++];
            } else {
                temp[(int)k++] = arr[(int)j++];
                inversionCount += (mid - i + 1); // Counting inversions
            }
        }

        while (i <= mid) {
            temp[(int)k++] = arr[(int)i++];
        }

        while (j <= right) {
            temp[(int)k++] = arr[(int)j++];
        }

        for (i = left; i <= right; i++) {
            arr[(int)i] = temp[(int)i];
        }
    }
}
